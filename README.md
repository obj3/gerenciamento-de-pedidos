# **Gerenciamento de Pedidos**
---
# Tabelas
  - gp_cliente
    - Tabela de cadastro de clientes
  - gp_produto
    - Tabela de cadastro de produtos
  - gp_pedido
    - Tabela de cadastro do cabecalho do pedido
  - gp_pedido_item
    - Tabela de cadastro dos itens do pedido
      - *Foi criado esta tabela para uma modelagem melhor quanto aos dados que se referem a um pedido e seus itens*


# Funcionalidades

As funcionalidades de criar, alterar, cancelar, totalizar e listar o historico de pedidos foram empacotadas
dentro da package pPedido.sql. Al�m disso, mais algumas funcionalidades de apoio e para melhor organizar o codigo foram adicionadas.

- PACKAGE pPedido - SPEC E BODY
  - procedure criar_pedido
    - Cria um novo pedido e seus referidos itens informando o cliente e o produto, ou cria apenas um novo item para um pedido j� existente, neste caso informa-se o id do pedido. Ap�s a inser��o do item do pedido � feita a atualiza��o do total do pedido e a atualiza��o do saldo em estoque do produto.
  - procedure atualizar_pedido
    - Informando o pedido e o produto, � possivel fazer a atualiza��o da data do pedido e da quantidade do item do pedido, considerando o estoque do produto mais a atual quantidade do item do pedido a ser alterada, para saber se ter� saldo suficiente para a nova quantidade informada. Ap�s isso, novamente � feito a atualiza��o do total do pedido e a atualiza��o do saldo em estoque do produto.
  - procedure cancelar_pedido
    - Atualiza o estoque dos produtos de cada item do pedido e muda o status do pedido para cancelado.
  - function calcular_total_pedido
    - Faz a somatoria da multiplica��o da quantidade pelo valor unitario de cada item do pedido e retorna o resultado.
  - function historico_pedidos_cliente
    - Faz uma consulta dos dados de um pedido e retorna o valor em um cursor para ser usado posteriormente um uma listagem.
  - lista_historico_pedidos_cliente
    - Faz uma listagem dos dados retornados da fun��o historico_pedidos_cliente
  - --
  - procedure retornaEstoque
    - Retorna a quantidade em estoque e o pre�o do produto
  - procedure atualizaEstoque
    - Faz a atualiza��o da quantidade em estoque do produto

# Scripts para testar as funcionalidades
obj/Scripts
## Scripts de atualiza��o de cadastros basicos
 - Insere_cliente.sql
 - Insere_produto.sql
## Scripts de manipula��o de dados de pedidos
 - Insere_pedido.sql
 - altera_pedido.sql
 - cancela_pedido.sql
 - lista_pedidos.sql


