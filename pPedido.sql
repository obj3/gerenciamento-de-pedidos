CREATE OR REPLACE 
PACKAGE pPedido AS 
  
procedure criar_pedido(pinIdCliente IN gp_cliente.id%TYPE, 
                       pinIdProduto IN gp_produto.id%TYPE,
                       pinQtd       IN gp_pedido_item.qtd%TYPE,
                       pinData      IN gp_pedido.data%TYPE,
                       pidPedido    IN gp_pedido.id%TYPE := NULL);

procedure atualizar_pedido(pidPedido    IN gp_pedido.id%TYPE,
                           pinIdProduto IN gp_produto.id%TYPE,
                           pinQtdNova   IN gp_pedido_item.qtd%TYPE,
                           pinDataNova  IN gp_pedido.data%TYPE);

procedure cancelar_pedido(pidPedido IN gp_pedido.id%TYPE);

function calcular_total_pedido(pidPedido IN gp_pedido.id%TYPE)
return NUMBER;

procedure atualiza_total_pedido (pidPedido IN gp_pedido.id%TYPE);

procedure retornaEstoque(pinIdProduto   IN gp_produto.id%TYPE,
                         ponQtdEstoque OUT gp_produto.qtd_estoque%TYPE,
                         ponPreco      OUT gp_produto.preco%TYPE);


procedure atualizaEstoque(pinIdProduto IN gp_produto.id%TYPE,
                          pinQtd       IN gp_produto.qtd_estoque%TYPE := NULL,
                          pQtdAtual    IN gp_produto.qtd_estoque%TYPE := NULL);

procedure lista_historico_pedidos_cliente(pidCliente IN gp_cliente.id%TYPE);

function historico_pedidos_cliente(pidCliente IN gp_cliente.id%TYPE)
return SYS_REFCURSOR;

END pPedido;