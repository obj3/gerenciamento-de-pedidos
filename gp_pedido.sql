DROP TABLE GP_PEDIDO
/


CREATE TABLE GP_PEDIDO
(
  ID NUMBER(8) NOT NULL 
, ID_CLIENTE NUMBER(8) NOT NULL 
, DATA DATE
, STATUS VARCHAR2(16)  
, VLR_TOTAL NUMBER(10,2)
, CONSTRAINT GP_PEDIDO_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
);

ALTER TABLE GP_PEDIDO
ADD CONSTRAINT GP_PEDIDO_FK1 FOREIGN KEY
(
  ID_CLIENTE
)
REFERENCES GP_CLIENTE
(
  ID 
)
ENABLE;

alter table GP_PEDIDO add constraint GP_PEDIDO_CHK1 check ( STATUS IN ('Em Processamento','Entregue','Cancelado')) ENABLE;

COMMENT ON COLUMN GP_PEDIDO.ID IS 'Identificador do pedido';

COMMENT ON COLUMN GP_PEDIDO.ID_CLIENTE IS 'Identificador do cliente do pedido';

COMMENT ON COLUMN GP_PEDIDO.DATA IS 'Data do pedido';

COMMENT ON COLUMN GP_PEDIDO.STATUS IS 'Status do pedido';

COMMENT ON COLUMN GP_PEDIDO.VLR_TOTAL IS 'Valor total do pedido';