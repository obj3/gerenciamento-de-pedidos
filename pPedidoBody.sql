CREATE OR REPLACE 
PACKAGE BODY pPedido AS 

  procedure criar_pedido (pinIdCliente IN gp_cliente.id%TYPE, 
                          pinIdProduto IN gp_produto.id%TYPE,
                          pinQtd       IN gp_pedido_item.qtd%TYPE,
                          pinData      IN gp_pedido.data%TYPE,
                          pidPedido    IN gp_pedido.id%TYPE := NULL)
  is
    vQtdEstoque  gp_produto.qtd_estoque%TYPE;
    vPreco       gp_produto.preco%TYPE;
    vIdPedido    gp_pedido.id%TYPE;
    vTotalPedido gp_pedido.vlr_total%TYPE;
  begin
  
    retornaEstoque(pinIdProduto, vQtdEstoque, vPreco);
  
    if vQtdEstoque >= pinQtd then
  
       begin
         
        if pidPedido is null then

          insert into gp_pedido ( ID, 
                                 ID_CLIENTE,
                                 DATA,
                                 STATUS,
                                 VLR_TOTAL)
                        values ( seq_pedido.nextval,
                                 pinIdCliente,
                                 pinData,
                                 'Em Processamento',
                                 NULL) returning ID into vIdPedido;
        else

          vIdPedido := pidPedido;

        end if;
  
        insert into gp_pedido_item ( ID, 
                                     ID_PEDIDO,
                                     ID_PRODUTO,
                                     QTD,
                                     VLR_UNIT)
                            values ( seq_pedido_item.nextval,
                                     vIdPedido,
                                     pinIdProduto,
                                     pinQtd,
                                     vPreco);
  
        
        atualiza_total_pedido(vIdPedido);
  
        atualizaEstoque(pinIdProduto, pinQtd);
        
        dbms_output.put_line('Pedido executado.');
  
       end;
  
    else
      dbms_output.put_line('Estoque insuficiente para o produto.');
    end if;
  end;
  
  procedure atualizar_pedido (pidPedido    IN gp_pedido.id%TYPE,
                              pinIdProduto IN gp_produto.id%TYPE,
                              pinQtdNova   IN gp_pedido_item.qtd%TYPE,
                              pinDataNova  IN gp_pedido.data%TYPE)
  is 
    vQtdPedidoAtual gp_pedido_item.qtd%type;
    vQtdEstoque     gp_produto.qtd_estoque%TYPE;
    vPreco          gp_produto.preco%TYPE;
  begin
  
      retornaEstoque(pinIdProduto, vQtdEstoque, vPreco);
  
      begin
  
        select qtd
          into vQtdPedidoAtual
          from gp_pedido_item
         where id_pedido = pidPedido
           and id_produto = id_produto;
  
      end;
  
      if vQtdEstoque + vQtdPedidoAtual >= pinQtdNova then
  
        update gp_pedido 
           set data = pinDataNova    
         where id   = pidPedido;
  
         update gp_pedido_item
            set qtd        = pinQtdNova
          where id_pedido  = pidPedido
            and id_produto = id_produto;
  
        atualiza_total_pedido(pidPedido);
        atualizaEstoque(pinIdProduto, pinQtdNova, vQtdPedidoAtual);
  
      else
  
        dbms_output.put_line('Estoque insuficiente para o produto.');
  
      end if;
  
  end atualizar_pedido;
  
  procedure cancelar_pedido(pidPedido IN gp_pedido.id%TYPE)
  is 
  
    vQtdTotalPedido gp_pedido_item.qtd%TYPE;
    
    cursor cQtdPorProdutoNoPedido is 
      select sum(qtd) qtd, id_produto
        from gp_pedido_item
       where id_pedido = pidPedido
    group by id_produto;
  
    rQtdPorProdutoNoPedido cQtdPorProdutoNoPedido%ROWTYPE;
  
  begin
    
    open cQtdPorProdutoNoPedido;
    loop
      fetch cQtdPorProdutoNoPedido into rQtdPorProdutoNoPedido;
      exit when cQtdPorProdutoNoPedido%NOTFOUND;
  
      atualizaEstoque(rQtdPorProdutoNoPedido.id_produto, NULL, rQtdPorProdutoNoPedido.qtd);    
  
    end loop;
  
    update gp_pedido
       set status = 'Cancelado'
     where id     = pidPedido;
    
  end cancelar_pedido;
  
  procedure retornaEstoque(pinIdProduto   IN gp_produto.id%TYPE,
                           ponQtdEstoque OUT gp_produto.qtd_estoque%TYPE,
                           ponPreco      OUT gp_produto.preco%TYPE)
  is 
  begin
    begin
      select pro.qtd_estoque, pro.preco
        into ponQtdEstoque, ponPreco
        from gp_produto pro
       where pro.id = pinIdProduto;
      exception
        when no_data_found then
          dbms_output.put_line('Produto n�o encontrato.');
    end;
    
  end retornaEstoque;
  
  procedure atualizaEstoque(pinIdProduto IN gp_produto.id%TYPE,
                            pinQtd       IN gp_produto.qtd_estoque%TYPE := NULL,
                            pQtdAtual    IN gp_produto.qtd_estoque%TYPE := NULL)
  is 
  begin
  
    begin
      update gp_produto
         set qtd_estoque = qtd_estoque + nvl(pQtdAtual,0) - nvl(pinQtd,0)
       where id = pinIdProduto;
    end;
  
  end atualizaEstoque;
  
  function calcular_total_pedido (pidPedido    IN gp_pedido.id%TYPE)
  return NUMBER
  is 
    vTotalPedido gp_pedido.vlr_total%type;
  begin
  
    begin
      select sum(qtd * vlr_unit)
        into vTotalPedido
        from gp_pedido_item
       where id_pedido = pidPedido;
    exception
      when no_data_found then
        dbms_output.put_line('Pedido n�o encontrato.');
    end;
    
    return (vTotalPedido);
  
  end calcular_total_pedido;
  
  procedure atualiza_total_pedido (pidPedido IN gp_pedido.id%TYPE)
  is 
  begin
  
    begin
      update gp_pedido
         set vlr_total = calcular_total_pedido(pidPedido)
       where id = pidPedido;
  
    end;
  
  end atualiza_total_pedido;
  
  procedure lista_historico_pedidos_cliente(pidCliente IN gp_cliente.id%TYPE)
  is 
    cHistoricoPedidos SYS_REFCURSOR;
    vId                gp_pedido.id%TYPE;
    vData              gp_pedido.data%TYPE;
    vStatus            gp_pedido.status%TYPE;
    vVlr_total         gp_pedido.vlr_total%TYPE;
  
  begin
    
    cHistoricoPedidos := historico_pedidos_cliente(pidCliente);
    loop
      fetch cHistoricoPedidos into vId, vData, vStatus, vVlr_total;
      exit when cHistoricoPedidos%NOTFOUND;
      dbms_output.put_line(vId||' '|| vData ||' '|| vStatus ||' '|| vVlr_total);
    end loop;
  
  end lista_historico_pedidos_cliente;
  
  function historico_pedidos_cliente(pidCliente IN gp_cliente.id%TYPE)
  return SYS_REFCURSOR
  is 
    cHistoricoPedidos SYS_REFCURSOR;
  begin
    open cHistoricoPedidos for
    select ped.id,
           ped.data,
           ped.status,
           ped.vlr_total
      from gp_pedido ped
     where ped.id_cliente = pidCliente;
  
    return (cHistoricoPedidos);  
    
  end historico_pedidos_cliente;

END pPedido;

